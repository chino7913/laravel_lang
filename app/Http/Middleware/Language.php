<?php

namespace App\Http\Middleware;

use Closure;
use App;
use Config;
use Session;

class Language
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $lang = $request->route('locale');

        if($lang AND array_key_exists(strtolower($lang), Config::get('app.locales'))){
            App::setLocale($lang);
            Session::put("applocale",strtolower($lang),"/");
        }
        elseif (Session::has('applocale') AND array_key_exists(Session::get('applocale'), Config::get('app.locales'))) {
            App::setLocale(Session::get('applocale'));
        }
        else { // This is optional as Laravel will automatically set the fallback language if there is none specified
            App::setLocale(Config::get('app.fallback_locale'));
            Session::put("applocale",Config::get('app.locale'),"/");
        }

        return $next($request);
    }
}