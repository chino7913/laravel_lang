<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/about', function () {
    return view('about');
});

/*多國語設定*/
Route::get('/{locale?}', function ($lang) {
    return view('welcome');
})->middleware('lang_of_web')->where('locale', 'en|tc|sc');

Route::group([
    'prefix' => '/{locale?}',
    'where' => ['locale' => 'en|tc|sc'],
    'middleware' => 'lang_of_web'
], function($lang) {
	
	Route::get('about', function ($lang) {
	    return view('about');
	});

});